import React from 'react';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import ForecastForm from './ForecastForm';
import { StyleSheet, Text, View } from 'react-native';

export default function Forecast() {
  return (
    <View>
          <ForecastTitle />
          <ForecastResult />
          <ForecastForm />
    </View>
  );
}

