import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default function ForecastResult(){
        return (
                <View style={styles.text}>  
                    <Text> 10°C </Text>  
                </View>
        );
}

const styles = StyleSheet.create({
    text: {
      color: 'pink',
      fontSize: 40,
      margin: 8,
    }
});