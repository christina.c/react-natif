import React from 'react';
import { fetchForecast } from '../actions/forecastAction';
import { StyleSheet, Text, View, Button, TextInput} from 'react-native';



class ForecastForm extends React.Component{

    
    constructor(props) {
        super(props);
        this.state = {
            'city' : 'Lyon',
        }
    }

    changeCity = (event) => {
        this.setState({
            'city' : event.target.value
        })
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    render(){
            return (
                <View>
                    <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => this.changeCity(text)}
                    value={this.state.city}/>
                
                <Button
                    onPress={this.fetchForecast}
                    title="Trouver votre ville"
                    color="#841584"
                    accessibilityLabel="meteo"/>
                </View>
            );
        }
    }

export default ForecastForm