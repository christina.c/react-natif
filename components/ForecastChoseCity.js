import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function ForecastChoseCity (){

   return (
       <View style={styles.text}>
          <Text>Bienvenue sur nôtre site</Text>
       </View>
            
    );
}

const styles = StyleSheet.create({
    text: {
      color: 'pink',
      fontSize: 40,
      margin: 30,
    }
});