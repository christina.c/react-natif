import React from 'react';
import ForecastChoseCity from './ForecastChoseCity';
import { StyleSheet, Text, View } from 'react-native';



export default function ForecastTitle (){

        return (
            <View style={styles.text}>
                <ForecastChoseCity />
                <Text>Lundi 2 sept</Text>
            </View>
        );
    }


const styles = StyleSheet.create({
    text: {
      color: 'pink',
      fontSize: 70,
      margin: 60,
    }
});