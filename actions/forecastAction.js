

export const updateForecast = (forecast) => {
    return {
        type: 'UPDATE_FORECAST',
        value: forecast
    }
  };
  
  export const toggleLoader = (status) => {
    return {
        type: 'TOGGLE_LOADER',
        value: status
    }
  };
  
  export const fetchForecast = (city) => {
      return async dispatch => {
        dispatch(toggleLoader(true));
        const response = await fetch('http://api.weatherstack.com/current?access_key=fa6d989dcd6feb1ea861926a763e99ef&query='+city);
        const data = await response.json();
        console.log(data);
        dispatch(updateForecast(data));
        dispatch(toggleLoader(false));
      }
  };