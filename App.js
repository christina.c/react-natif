import React from 'react';
import { Provider } from 'react-redux'
import Forecast from './components/Forecast';
import store from './store';


export default function App() {
  return (
    <Provider store={store}>
      <Forecast />
    </Provider>
  );
}